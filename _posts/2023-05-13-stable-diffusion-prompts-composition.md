---
layout: post
title: An exhaustive list of composition elements for stable diffusion prompts. With examples
excerpt-separator:  <!--more-->
---

![alt text](/assets/tree.png "Military")

* Subject
  * Person: portrait, self-portrait, family portrait, candid shot, street photography, fashion photography, action shot, posing, dance photography, group photo
  * Animal: wildlife photography, pet portrait, zoo photography, animal action shot, bird photography, underwater animal photography, farm animal photography, animal close-up, animal in motion
  * Landscape: cityscape, seascape, mountain landscape, rural landscape, desert landscape, forest landscape, sunset/sunrise, aerial view, beachscape


* Verb
  * Standing: portrait, street photography, fashion photography, outdoor sculpture photography, event photography, protest photography, architectural photography, landscape photography, candid shot
  * Sitting: portrait, lifestyle photography, outdoor photography, wedding photography, family photography, street photography, candid shot, pet photography, fashion photography
  * Eating: food photography, food styling photography, restaurant photography, lifestyle photography, event photography, street photography, candid shot, still life photography, cooking photography


* Adjectives
  * Beautiful: nature photography, fashion photography, landscape photography, portrait photography, wedding photography, still life photography, macro photography, fine art photography, street photography, travel photography
  * Realistic: wildlife photography, architectural photography, portrait photography, product photography, scientific photography, historical photography, sports photography, street photography, documentary photography, landscape photography
  * Big: cityscape photography, nature photography, architectural photography, aerial photography, landscape photography, sports photography, street photography, travel photography, wildlife photography, event photography

<!--more-->
* Environment/Context
    * Outdoor: landscape photography, nature photography, sports photography, event photography, street photography, fashion photography, travel photography, architectural photography, protest photography, candid shot
    * Underwater: ocean photography, marine life photography, scuba diving photography, aquarium photography, surf photography, swimming photography, underwater portrait photography, underwater fashion photography, underwater action photography, abstract underwater photography
    * In the sky: aerial photography, sky photography, aviation photography, drone photography, hot air balloon photography, paragliding photography, skydiving photography, cloud photography, star photography, aurora borealis photography
    * At night: night photography, astrophotography, cityscape photography, light painting photography, fireworks photography, long exposure photography, street photography, urban exploration photography, star trail photography, neon sign photography


* Lighting
    * Soft: portrait photography, fashion photography, still life photography, macrophotography, wedding photography, product photography, food photography, baby photography, newborn photography
    * Ambient: landscape photography, cityscape photography, street photography, travel photography, architectural photography, interior photography, night photography, event photography, concert photography
    * Neon: night photography, cityscape photography, street photography, abstract photography, fashion photography, portrait photography, event photography, architecture photography, product photography, urban exploration photography
    * Foggy: landscape photography, cityscape photography, street photography, travel photography, night photography, event photography, portrait photography, fashion photography, concert photography


* Emotions
  * Cosy: lifestyle photography, home decor photography, food photography, family photography, pet photography, landscape photography, still life photography, winter photography, autumn photography
  * Energetic: sports photography, action photography, dance photography, concert photography, festival photography, street photography, travel photography, event photography, lifestyle photography, fitness photography
  * Romantic: wedding photography, engagement photography, couple photography, portrait photography, fashion photography, fine art photography, travel photography, event photography, lifestyle photography, still life photography
  * Grim: dark photography, horror photography, documentary photography, war photography, abandoned places
  * Loneliness: street photography, urban exploration photography, landscape photography, cityscape photography, portrait photography, travel photography, documentary photography, abandoned places photography, fine art photography
  * Fear: horror photography, abandoned places photography, dark photography, nature photography, wildlife photography, street photography, documentary photography, architecture photography, urban exploration photography


* Artist Inspiration
    * Pablo Picasso: cubism, abstract art, portrait painting, still life painting, sculpture, printmaking, drawing
    * Van Gogh: post-impressionism, landscape painting, portrait painting, still life painting, drawing, printmaking
    * Da Vinci: renaissance art, portrait painting, anatomy drawing, scientific drawings, sculpture, invention sketches
    * Hokusai: ukiyo-e art, landscape painting, nature painting, portrait painting, printmaking


* Art Medium
  * Oil on canvas: traditional painting medium, suitable for portrait painting, landscape painting, still life painting, abstract art, fine art painting, contemporary art
  * Watercolour: transparent painting medium, suitable for landscape painting, nature painting, still life painting, portrait painting, abstract art, botanical illustration, scientific illustration
  * Sketch: pencil or charcoal drawing medium, suitable for portrait drawing, figure drawing, still life drawing, landscape drawing, architectural drawing, conceptual drawing, comic art
  * Photography: visual medium for capturing images, suitable for portrait photography, landscape photography, documentary photography, fashion photography, wildlife photography, travel photography, street photography, sports photography, fine art photography


* Photography Style
    * Polaroid: vintage instant camera style, suitable for portrait photography, still life photography, travel photography, urban photography, street photography, fashion photography, landscape photography, abstract photography
    * Long exposure: photography style that uses a long shutter speed, suitable for night photography, landscape photography, cityscape photography, astrophotography, seascape photography, waterfall photography, light painting photography
    * Monochrome: photography style that uses black and white or shades of one colour, suitable for portrait photography, street photography, architecture photography, landscape photography, fine art photography, documentary photography
    * GoPro: action camera style, suitable for sports photography, adventure photography, underwater photography, travel photography, action photography, outdoor photography
    * Fisheye: photography style that uses a wide-angle lens with a distorted effect, suitable for landscape photography, architecture photography, portrait photography, street photography, fine art photography, experimental photography
    * Bokeh: photography style that uses a shallow depth of field to create a blurred background, suitable for portrait photography, still life photography, product photography, fashion photography, nature photography, macro photography


* Art Style
    * Manga: Japanese comic book style, suitable for comic art, graphic novels, illustration, animation, character design, gaming
    * Fantasy: imaginative and fictional style, suitable for illustration, concept art, fantasy art, gaming, animation, comics
    * Minimalism: simplistic and minimalist style, suitable for graphic design, illustration, typography, branding, fine art, web design, product design
    * Abstract: non-representational and experimental style, suitable for fine art, contemporary art, painting, sculpture, photography, installation art
    * Graffiti: urban and street art style, suitable for public art, mural art, graphic design, illustration, typography, printmaking, fine art


* Material
  * Fabric: textile material, suitable for fashion design, textile art, sewing, embroidery, quilting, weaving, printing
  * Wood: natural material, suitable for woodworking, furniture design, sculpture, carving, woodturning, wood burning, woodcut printmaking
  * Clay: malleable material, suitable for pottery, sculpture, ceramics, modeling, tile making, jewelry making, figurine making


* Colour Scheme
  * Pastel: soft and muted colour scheme, suitable for portrait photography, still life photography, wedding photography, fashion photography, interior design, graphic design, web design, branding
  * Vibrant: bold and saturated colour scheme, suitable for landscape photography, nature photography, street photography, product photography, advertising, illustration, branding, graphic design
  * Dynamic Lighting: lighting style that creates contrast and dramatic effect, suitable for portrait photography, fashion photography, product photography, cinematic photography, fine art photography, architecture photography, night photography, landscape photography


* Computer Graphics
  * 3D: computer graphics that create a 3D model, suitable for animation, gaming, architectural visualization, product design, visual effects, character design
  * Octane: high-end rendering software, suitable for film and video production, animation, visual effects, product visualization, architectural visualization, automotive visualization
  * Cycles: rendering engine for 3D software, suitable for animation, architectural visualization, product visualization, scientific visualization, visual effects, character design


* Illustrations
  * Isometric: 3D illustration style that uses a 30-degree angle, suitable for technical illustration, architectural visualization, product design, infographic design, game design, advertising
  * Pixar: animation studio known for its 3D animation style, suitable for animation, character design, concept art, visual effects, product visualization, advertising, gaming
  * Scientific: illustration style that focuses on accuracy and detail, suitable for scientific illustration, medical illustration, nature illustration, botanical illustration, zoological illustration, anatomical illustration
  * Comic: illustration style that uses panels and speech bubbles, suitable for comic art, graphic novels, editorial illustration, advertising, character design, animation


* Quality
    * High Definition: resolution of 720p or higher, suitable for television, film, video production, gaming, animation, live events, sports
    * 4K: resolution of 3840x2160 pixels or higher, suitable for film, television, video production, gaming, animation, visual effects, sports
    * 8K: resolution of 7680x4320 pixels or higher, suitable for film, television, video production, gaming, animation, visual effects, sports

