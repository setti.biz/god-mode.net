---
layout: post
title: Почему этот сайт работает на статическом генераторе контента?
excerpt_separator:  <!--more-->
---
![alt text](/assets/stat.jpeg "Static sites generators")

#### Причин более, чем достаточно. Вот некоторые из них:

* 🧑‍💻 Никаких баз данных!
* 🧑‍💻 Фреймворки уязвимы
* 🧑‍💻 Обычные фреймворки надо обновлять
* 🧑‍💻 В случае классического сайта на меня ложиться дополнительная нагрузка по развёртыванию, поддержке окружения
* 🧑‍💻 Портативность именно статических сайтов трудно переоценить. Движков для автогенов очень много и все они способны работать с тем форматом файлов, в которых у меня хранится отформатированный текст - Markdown
* 🧑‍💻 Способ хранения данных для таких вот автогенов - это набор Markdown файлов. Они переносятся на любой другой подобный автоген с полпинка. А движков для автогенов много
* 🧑‍💻 Всё управление контентом происходит в Git и IDE. Мне очень удобно
* 🧑‍💻 Такие сайты очень быстрые, их невозможно перегрузить или сломать
* 🧑‍💻 Переезд на новый сервер - это что-то из разряда "ничего даже делать для этого не надо"
* 🧑‍💻 В облаке, где крутится вся моя серверная инфраструктура (DigitalOcean) такие сайты размещаются и работаю бесплатно. DigitalOcean из коробки предлагает функционал автогена для моего автоген-движка. Автоген отрабатывает на этапе, когда в Git появляется новый код моих Markdown файлов. После того, как автоген отработал на этапе сборки сайта, готовый статический сайт отправляется на сервер и там он работает без какого либо языка, движка, базы и прочего
