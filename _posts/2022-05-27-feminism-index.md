---
layout: post
title: Индекс феминизма
excerpt_separator:  <!--more-->
---
Доля людей в разных странах, кто согласен, что высшее образование важнее для мальчиков, чем для девочек _(A university education is more important for a boy than for a girl)_. С группировкой по странам. Данные из World Values Survey Wave 7 (2017-2022)

| COUNTRY       |   % |
|:--------------|----:|
| Andorra       |   3 |
| Australia     |   3 |
| New Zealand   |   3 |
| Germany       |   4 |
| Puerto Rico   |   7 |
| Canada        |   7 |
| Greece        |   8 |
| United States |   9 |
| Brazil        |  10 |
| Serbia        |  10 |
| Taiwan ROC    |  11 |
| Japan         |  12 |
| Argentina     |  14 |
| Peru          |  14 |
| Zimbabwe      |  14 |
| Lebanon       |  15 |
| Guatemala     |  15 |
| Cyprus        |  16 |
| Ethiopia      |  16 |
| Singapore     |  17 |
| Kenya         |  18 |
| Venezuela     |  18 |
| Colombia      |  18 |
| Armenia       |  18 |
| Hong Kong SAR |  18 |
| Mexico        |  19 |
| Romania       |  19 |
| Morocco       |  20 |
| Nicaragua     |  21 |
| China         |  21 |
| Bolivia       |  22 |
| Ecuador       |  22 |
| Ukraine       |  23 |
| Macau SAR     |  23 |
| Chile         |  23 |
| Jordan        |  24 |
| Tunisia       |  25 |
| Russia        |  27 |
| Vietnam       |  28 |
| Kazakhstan    |  28 |
| Egypt         |  30 |
| Libya         |  31 |
| Thailand      |  31 |
| Iraq          |  32 |
| Mongolia      |  32 |
| Turkey        |  32 |
| South Korea   |  34 |
| Malaysia      |  36 |
| Nigeria       |  41 |
| Bangladesh    |  43 |
| Philippines   |  44 |
| Indonesia     |  44 |
| Iran          |  47 |
| Tajikistan    |  52 |
| Kyrgyzstan    |  52 |
| Myanmar       |  52 |
| Pakistan      |  59 |
