---
layout: post
title: Чем люди хуже роботов
excerpt_separator:  <!--more-->
---
* People generally require a degree of training.
* We typically want more over time.
* We need to rest.
* We require health care that you sometimes must pay for and we can be very particular about.
* We get sick.
* We want to feel good about what we’re doing.
* We have bad days.
* We can’t do the same task precisely the same way millions of times.
* We have families who we want to spend time with.
* We are sometimes bad at our jobs and need to be fired. In which case we generally want severance pay or we will make you feel bad.
* We get bored.
* We have legal protections. We occasionally sue our employers.
* We can become demoralized and unproductive.
* We take 15–20 years of rearing to become productive and then we are unproductive and infirm for 10–15 years at the back ends of our lives. We often want you to pay us to account for both the time at the end and the cost of raising our children.
* If something bad happens to one of us, the others notice.
* We occasionally harass each other or sleep with each other.
* We sleep.
* We sometimes are dishonest and even steal.
* We occasionally quit and look for other jobs.
* We see things. We share information.
* Some of us use drugs.
* We get injured and disabled.
* We are unreliable. We sometimes change our minds.
* We sometimes take breaks when we should be working.
* We sometimes organize and negotiate for various benefits beyond what we could get on our own.
* We sometimes have bad judgment and can act in ways that will tarnish your brand.
* We have social media accounts.
* We expect time off for holidays.
* We sometimes get divorced or have relationships end which can make us sad and unproductive.
* We sometimes talk to reporters.
* You cannot sell us to another firm.
* We do not come with warranties.
* Our software often does not upgrade easily.

_from
The War on Normal People
by Andrew Yang_
