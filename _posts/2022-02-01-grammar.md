---
layout: post
title: О чём говорит способность или неспособность правописания?
excerpt_separator:  <!--more-->
---
Не всем нам так уж очевидно, зачем писать правильно. Но эволюционно люди научились делать суждения на основании способностей излагать мысли текстом. О чем можно судить по правописанию человека?

* 🥏 Об уровне культурного развития
* 🥏 Об усилиях человека в его желании быть понятым и в целом об эмпатии, соответственно. Психам по фонарю, понимаете вы их или нет.
* 🥏 О понимании человеком восприятия его речи окружающими. Если ему всё равно, то и стараться правильно писать он не будет.
* 🥏 Об уважении/неуважении к тем, к кому он обращается.
* 🥏 Корреляция правописания с эрудицией существенная. Так что судить по правописанию о начитанности можно.
* 🥏 Об уровне образования. Необразованные люди опасны для общества. Именно поэтому зачастую ошибки в тексте "режут глаз"

Стоит уточнить, что для людей, пишущих не на своём родном языке, эти суждения могут быть сильно ошибочными. 
