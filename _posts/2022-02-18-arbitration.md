---
layout: post
title: Арбитраж или спекуляция?
excerpt_separator:  <!--more-->
---

![kinky-questionary-reports](/assets/arbitration.jpeg)

Когда-то давно, когда я был маленьким и наше общество только входило в режим рыночной экономики, я спросил у дедушки, чем плохо новое положение дел. Он ответил, что теперь появились спекулянты — плохие люди, которые покупают подешевле и продают подороже. Так в моей голове зародилась негативная коннотация спекуляции. Еще через какое-то количество лет придёт понимание, что спекуляции — это не плохо. А сегодня прочитал самое лаконичное обоснование пользы от спекулянтов:

> Арбитраж приводит к достаточно быстрому выравниванию цен между разными рынками и поддержанию их равновесия. Покупая дешёвый товар, арбитражеры создают дополнительный спрос на него и повышают тем самым его цену. Продавая этот товар на другом рынке, они увеличивают его предложение и снижают цену. #
> 
> — wiki

